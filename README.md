# Магистерска теза со наслов Унапредување на визуелен алгоритам за симултано локализирање и мапирање

### Инсталација:
Инсталирајте прво Tex
```bash
sudo dnf install -y texlive-scheme-full
```
Потоа симнете ги податоците
```bash
git clone https://gitlab.com/stefan.zlatinov/MasterThesisTex.git
```
За да ја изградите магистерската теза
```bash
cd MasterThesisTex
xelatex Магистерска.tex
evince Магистерска.pdf
```
или пак отворете со Texmaker
```bash 
sudo dnf install texmaker
```
и користете XeLatex.
